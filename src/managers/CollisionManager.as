package managers
{
	import flash.geom.Point;
	
	import core.Assets;
	
	import objects.Alien;
	import objects.Bullet;
	import objects.Score;
	
	import states.Play;

	public class CollisionManager {
		
		private var play:Play;
		private var p1:Point = new Point();
		private var p2:Point = new Point();
		
		public function CollisionManager(play:Play) {
			this.play = play;
		}
		
		public function update():void {
			bulletsAndAliens();
			multishotAndAliens();
			heroAndAliens();
		}
		
		private function heroAndAliens():void {
			var aa:Array = play.alienManager.aliens;
			var a:Alien;
			
			for (var i:int = aa.length-1; i >= 0; i--) {
				a = aa[i];
				p1.x = play.hero.x;
				p1.y = play.hero.y;
				p2.x = a.x;
				p2.y = a.y;
				
				if (Point.distance(p1, p2) < a.pivotX + play.hero.pivotX + 20) {
					
					/* HERO AND ALIEN COLLISION */
					//1. Animate Hero
					//2. Screen Shake
					Score.setHealth(-20);
					
					play.explosionManager.spawn(a.x, a.y);
					play.alienManager.destroyAlien(a);
				}
			}
		}
		
		private function bulletsAndAliens():void {
			var ba:Array = play.blastManager.bullets;
			var aa:Array = play.alienManager.aliens;
			
			var b:Bullet;
			var a:Alien;
			
			for (var i:int = ba.length-1; i >= 0; i--) {
				
				b = ba[i];
				
				for (var j:int = aa.length-1; j >= 0; j--) {
					a = aa[j];
					p1.x = b.x;
					p1.y = b.y;
					p2.x = a.x;
					p2.y = a.y;
					
					if (Point.distance(p1, p2) < a.pivotX + b.pivotX) {						
						/* BULLET AND ALIEN COLLISION */
						Score.setExp(100);
						Score.setGold(Math.ceil(Math.random()*3));
						
						//Assets.explosion.play();
						play.explosionManager.spawn(a.x, a.y);
						play.alienManager.destroyAlien(a);
						play.blastManager.destroyBullet(b);
					}
				}
			}
		}
		
		private function multishotAndAliens():void {
			var ma:Array = play.multishotManager.bullets;
			var aa:Array = play.alienManager.aliens;
			
			var m:Bullet;
			var a:Alien;
			
			for (var i:int = ma.length-1; i >= 0; i--) {
				
				m = ma[i];
				
				for (var j:int = aa.length-1; j >= 0; j--) {
					a = aa[j];
					p1.x = m.x;
					p1.y = m.y;
					p2.x = a.x;
					p2.y = a.y;
					
					if (Point.distance(p1, p2) < a.pivotX + m.pivotX) {						
						/* MULTISHOT AND ALIEN COLLISION */
						Score.setExp(100);
						Score.setGold(Math.ceil(Math.random()*3));
						
						//Assets.explosion.play();
						play.explosionManager.spawn(a.x, a.y);
						play.alienManager.destroyAlien(a);
					}
				}
			}
		}
	}
}