package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import objects.Bullet;
	import objects.Score;
	
	import states.Play;

	public class BlastManager {
		
		private var play:Play;
		public var bullets:Array;
		private var pool:StarlingPool;
		private var fireDelay:Timer;
		private var canFire:Boolean = true;
		private var upgradeLevel:Dictionary;
		
		public static var currentLevel:uint;
		
		public function BlastManager(play:Play) {
			this.play = play;
			bullets = new Array();
			pool = new StarlingPool(Bullet, 10);
			
			currentLevel = 0;
			
			//Delay in MS
			upgradeLevel = new Dictionary();
			upgradeLevel[0] = 700;
			upgradeLevel[1] = 500;
			upgradeLevel[2] = 300;
			upgradeLevel[3] = 100;
		}
		
		public function update():void {
			
			fireDelay = new Timer(upgradeLevel[currentLevel], 1);//CHANGE DEPENDING ON UPGRADE LEVEL 700/500/300/150
			fireDelay.addEventListener(TimerEvent.TIMER, onFireTimer);
			
			var b:Bullet;
			var len:int = bullets.length;
			
			for (var i:int = len-1; i >= 0; i--) {
				b = bullets[i];
				b.x += 25;
				
				if (b.x > 800) {
					destroyBullet(b);
				}
			}
			
			if (play.fireBlast && canFire && Score.getEnergy() > 0) {
				fire()
				canFire = false;
				fireDelay.start();
				
				//ENERGY COST
				switch (currentLevel) {
					case 0 : Score.setEnergy(-3);
						break;
					case 1 : Score.setEnergy(-2);
						break;
					case 2 : Score.setEnergy(-1);
						break;
					case 3 : Score.setEnergy(0);
						break;
				}
			}
		}
		
		/* FIRE TIMER EVENT */
		private function onFireTimer(e:TimerEvent):void {
			canFire = true;
		}
		
		private function fire():void {
			var b:Bullet = pool.getSprite() as Bullet;
			
			play.addChild(b);
			b.x = play.hero.x + 20;
			b.y = play.hero.y;
			
			bullets.push(b);
			
//			Assets.shoot.play();
		}
		
		public function destroyBullet(b:Bullet):void {
			var len:int = bullets.length;
			
			for (var i:int = 0; i < len; i++) {
				if (bullets[i] == b) {
					bullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		public function destroy():void {
			pool.destroy();
			pool = null;
			bullets = null;
		}
	}
}