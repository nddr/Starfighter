package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Background extends Sprite {
		
		private var sky1:Image;
		private var sky1b:Image;
		private var sky2:Image;
		private var sky2b:Image;
		
		public function Background() {
//			sky2 = new Image(Assets.skyTexture2);
//			sky2.x = 400;
//			sky2.y = -100;
//			addChild(sky2);
//			
//			sky2b = new Image(Assets.skyTexture2);
//			sky2b.x = 1600;
//			sky2b.y = -100;
//			addChild(sky2b);
//			
//			sky1 = new Image(Assets.skyTexture);
//			sky1.x = 800;
//			sky1.y = 150;
//			addChild(sky1);
//			
//			sky1b = new Image(Assets.skyTexture);
//			sky1b.x = 2000;
//			sky1b.y = 150;
//			addChild(sky1b);
		}
		
		public function update():void {
			//Smaller/Bottom Planets
			sky1.x -= .4;
			sky1b.x -= .4;
			if (sky1.x <= -1000) {
				sky1.x = 1200;
			}
			if (sky1b.x <= -1000) {
				sky1b.x = 1200;
			}
			
			//Top/Bigger Planets
			sky2.x -= .1;
			sky2b.x -= .1;
			if (sky2.x <= -1000) {
				sky2.x = 1200;
			}
			if (sky2b.x <= -1000) {
				sky2b.x = 1200;
			}
		}
	}
}