package states
{
	import flash.ui.Keyboard;
	
	import core.Game;
	
	import interfaces.IState;
	
	import managers.AlienManager;
	import managers.BlastManager;
	import managers.CollisionManager;
	import managers.ExplosionManager;
	import managers.MultishotManager;
	import managers.StarManager;
	
	import objects.HeadsUp;
	import objects.Hero;
	import objects.NativeCursor;
	import objects.Score;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	
	public class Play extends Sprite implements IState {
		
		public var game:Game;
		public var hero:Hero;
		public var headsUp:HeadsUp;
		public var score:Score;
		public var fireBlast:Boolean;
		public var fireMultishot:Boolean;
		private var s:Stage;
		
		public var blastManager:BlastManager;
		public var multishotManager:MultishotManager;
		public var alienManager:AlienManager;
		private var collisionManager:CollisionManager;
		public var explosionManager:ExplosionManager;
		private var starManager:StarManager;
		private var cursor:NativeCursor;
		
		public function Play(game:Game) {
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void {
			cursor = new NativeCursor();
			
			s = Starling.current.stage;
			
			hero = new Hero(this);
			addChild(hero);
			
			score = new Score(this);
			
			headsUp = new HeadsUp(this);
			addChild(headsUp);
			
			blastManager = new BlastManager(this);
			multishotManager = new MultishotManager(this);
			alienManager = new AlienManager(this);
			collisionManager = new CollisionManager(this);
			explosionManager = new ExplosionManager(this);
			starManager = new StarManager(this);
			
			s.addEventListener(KeyboardEvent.KEY_DOWN, onDown);
			s.addEventListener(KeyboardEvent.KEY_UP, onUp);
		}
		
		private function onDown(e:KeyboardEvent):void {
			if (e.keyCode == Keyboard.SPACE) {
				fireBlast = true;
			}
			
			if (e.keyCode == Keyboard.Z) {
				fireMultishot = true;
			}
		}
		
		private function onUp(e:KeyboardEvent):void {
			if (e.keyCode == Keyboard.SPACE) {
				fireBlast = false;
			}
			
			if (e.keyCode == Keyboard.Z) {
				fireMultishot = false;
			}
			
		}
		
		public function update():void {
			hero.update();
			blastManager.update();
			multishotManager.update();
			alienManager.update();
			collisionManager.update();
			starManager.update();
			headsUp.update();
			score.update();
		}
		
		public function destroy():void {
			s.removeEventListener(KeyboardEvent.KEY_DOWN, onDown);
			s.removeEventListener(KeyboardEvent.KEY_UP, onUp);
			
			blastManager.destroy();
			multishotManager.destroy();
			alienManager.destroy();
			starManager.destroy();
			headsUp.destroy();
			removeFromParent(true);
		}
	}
}