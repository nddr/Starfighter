package objects
{
	import starling.display.Sprite;
	import flash.events.MouseEvent;
	
	public class KillContextMenu extends Sprite
	{
		public function KillContextMenu() {
			stage.addEventListener(MouseEvent.RIGHT_CLICK, doNothing);
		}
		
		private function doNothing(e:MouseEvent):void {
			//do nothing...
			trace(e.type);
		}
	}
}