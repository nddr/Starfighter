package core
{
	import flash.display.Sprite;
	import starling.core.Starling;
	
	[SWF(width="800", height="400", frameRate="60", backgroundColor="#00001A")]
	
	public class Starfighter extends Sprite {
		
		private var _star:Starling;
		
		public function Starfighter() {
			_star = new Starling(Game, stage);
			_star.showStats = true;
			_star.start();
		}
	}
}