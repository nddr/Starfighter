package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Star extends Sprite {
		
		public function Star() {
			var img:Image = new Image(Assets.ta.getTexture("star"));
			pivotX = width * 0.5;
			pivotY = height * 0.5;
			addChild(img);
		}
	}
}