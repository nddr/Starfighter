package objects
{
	import core.Game;
	import states.Play;

	public class Score {
		
		private static var exp:int = 0;
		private static var level:int = 1;
		private static var skp:int = 1;
		private static var health:int = 100;
		private static var energy:int = 200;
		private static var shield:Boolean = false;
		private static var gold:int = 0;
		
		public static var blastLevel:int = 0;
		public static var multishotLevel:int = 0;
		public static var missilesLevel:int = 0;
		
		private var play:Play;
		
		public function Score(play:Play) {
			this.play = play;
		}
		
		public function update():void {
			/* END GAME - RESET VARIABLES */
			if (health <= 0) {
				play.game.changeState(Game.GAME_OVER_STATE);
				
				exp = 0;
				level = 1;
				skp = 1;
				health = 100;
				energy = 100;
				shield = false;
				gold = 0;
			}
			
			switch (true) {
				case exp >= 800:
					level = 2;
					break;
				case exp >= 1680:
					level = 3;
					skp = 1;
					break;
				case exp >= 3696:
					level = 4;
					break;
				case exp >= 8500:
					level = 5;
					break;
				case exp >= 20400:
					level = 6;
					break;
				case exp >= 51000:
					level = 7;
					break;
			}
		}
		
		public static function getExp():int {
			return exp;
		}

		public static function setExp(value:int):void {
			exp += value;
		}
		
		public static function getLevel():int {
			return level;
		}
		
		public static function getSkillPts():int {
			return skp;
		}
		
		public static function setSkillPts(value:int):void {
			skp += value;
		}

		public static function getHealth():int {
			return health;
		}

		public static function setHealth(value:int):void {
			health += value;
		}

		public static function getEnergy():int {
			return energy;
		}

		public static function setEnergy(value:int):void {
			energy += value;
			if (energy < 0) {
				energy = 0;
			}
		}

		public static function getShield():Boolean {
			return shield;
		}

		public static function setShield(value:Boolean):void {
			shield = value;
		}
		
		public static function getGold():int {
			return gold;
		}
		
		public static function setGold(value:int):void {
			gold += value;
		}

	}
}