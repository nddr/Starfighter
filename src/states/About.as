package states
{
	import core.Assets;
	import core.Game;
	
	import interfaces.IState;
	
	import objects.NativeCursor;
	
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class About extends Sprite implements IState {
		
		private var game:Game;
		private var cursor:NativeCursor;
		private var info:TextField;
		private var back:Button;
		
		public function About(game:Game) {
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function init():void {
			cursor = new NativeCursor();
			
			info = new TextField(100, 30, "About Info", "Orator Std", 12, 0xFF0000);
			info.x = 700;
			info.y = 0;
			addChild(info);
			
			back = new Button(Assets.ta.getTexture("back"));
			back.addEventListener(Event.TRIGGERED, onBack);
			back.pivotX = back.width * 0.5;
			back.x = 400;
			back.y = 300;
			addChild(back);
		}
		
		private function onBack(e:Event):void {
			back.removeEventListener(Event.TRIGGERED, onBack);
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void {
		}
		
		public function destroy():void {
			removeFromParent(true);
		}
	}
}