package core
{
	import flash.media.Sound;
	import flash.media.SoundTransform;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class Assets {
		
		[Embed(source="/../assets/images/atlas.png")]
		private static var atlas:Class;
		public static var ta:TextureAtlas;
		
		[Embed(source="/../assets/images/atlas.xml", mimeType="application/octet-stream")]
		public static var atlasXML:Class;
		
		[Embed(source="/../assets/particles/space.pex", mimeType="application/octet-stream")]
		public static var spaceXML:Class;
		
		[Embed(source="/../assets/particles/exhaust.pex", mimeType="application/octet-stream")]
		public static var exhaustXML:Class;
		
		[Embed(source="/../assets/particles/explosion.pex", mimeType="application/octet-stream")]
		public static var explosionXML:Class;
		
//		[Embed(source="/../assets/sounds/explosion_small.mp3")]
//		private static var explosionSound:Class;
//		public static var explosion:Sound;
		
		public static function init():void {
			
			ta = new TextureAtlas(Texture.fromBitmap(new atlas()),
				XML(new atlasXML()));
			
//			explosion = new explosionSound();
//			exploion.play(0,0, new SoundTransform(0));
			
//			shoot = new shootSound();
//			shoot.play(0,0, new SoundTransform(0));
		}
	}
}