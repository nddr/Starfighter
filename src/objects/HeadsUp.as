package objects
{
	import core.Assets;
	
	import managers.BlastManager;
	import managers.MultishotManager;
	import managers.MissilesManager;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	import states.Play;

	public class HeadsUp extends Sprite {
		
		private var play:Play;
		
		private var health:TextField;
		private var energy:TextField;
		private var level:TextField;
		private var skillpt:TextField;
		private var gold:TextField;
		
		private var blastIcon:Button;
		private var multishotIcon:Button;
		private var missilesIcon:Button;
		
		private var blastLevel:TextField;
		private var multishotLevel:TextField;
		private var missilesLevel:TextField;
		
		public function HeadsUp(play:Play) {
			this.play = play;
			
			var box:Image = new Image(Assets.ta.getTexture("hudbox"));
			addChild(box);
			box.x = 15;
			box.y = 345;
			
			/* Textfields - Ability Levels */
			blastLevel = new TextField(30, 20, "0", "Orator Std", 16, 0xFFFFFF);
			play.addChild(blastLevel);
			blastLevel.x = 30;
			blastLevel.y = 35;
			
			multishotLevel = new TextField(30, 20, "0", "Orator Std", 16, 0xFFFFFF);
			play.addChild(multishotLevel);
			multishotLevel.x = 80;
			multishotLevel.y = 35;
			
			missilesLevel = new TextField(30, 20, "0", "Orator Std", 16, 0xFFFFFF);
			play.addChild(missilesLevel);
			missilesLevel.x = 130;
			missilesLevel.y = 35;
			
			/* Clickables - Ability Icons */
			blastIcon = new Button(Assets.ta.getTexture("blastIcon"));
			play.addChild(blastIcon);
			blastIcon.x = 10;
			blastIcon.y = 10;
			blastIcon.alpha = .4;
			
			multishotIcon = new Button(Assets.ta.getTexture("multishotIcon"));
			play.addChild(multishotIcon);
			multishotIcon.x = 60;
			multishotIcon.y = 10;
			multishotIcon.alpha = .4;
			
			missilesIcon = new Button(Assets.ta.getTexture("missilesIcon"));
			play.addChild(missilesIcon);
			missilesIcon.x = 110;
			missilesIcon.y = 10;
			missilesIcon.alpha = .4;
			
			/* Textfields - Vitals */
			health = new TextField(60, 30, "100", "Orator Std", 22, 0xd44e5d);
			play.addChild(health);
			health.x = 55;
			health.y = 360;
			
			energy = new TextField(60, 30, "100", "Orator Std", 22, 0x4ea0d4);
			play.addChild(energy);
			energy.x = 100;
			energy.y = 360;
			
			level = new TextField(40, 30, "1", "Orator Std", 22, 0xFFFFFF, true);
			play.addChild(level);
			level.x = 15;
			level.y = 360;
			
			skillpt = new TextField(20, 30, "0", "Orator Std", 16, 0xFFFFFF);
			play.addChild(skillpt);
			skillpt.x = 37;
			skillpt.y = 347;
			
			gold = new TextField(60, 30, "0", "Orator Std", 22, 0xD4C54E);
			play.addChild(gold);
			gold.x = 10;
			gold.y = 315;
		}
		
		private function onBlastIconClick(e:Event):void {
			BlastManager.currentLevel += 1;
			Score.setSkillPts(-1);
			blastIcon.removeEventListener(Event.TRIGGERED, onBlastIconClick);
		}
		
		private function onMultishotIconClick(e:Event):void {
			MultishotManager.currentLevel += 1;
			Score.setSkillPts(-1);
			multishotIcon.removeEventListener(Event.TRIGGERED, onBlastIconClick);
		}
		
		private function onMissilesIconClick(e:Event):void {
			MissilesManager.currentLevel += 1;
			Score.setSkillPts(-1);
			missilesIcon.removeEventListener(Event.TRIGGERED, onMissilesIconClick);
		}
		
		public function update():void {
			/* Update TextFields */
			health.text = String(Score.getHealth());
			energy.text = String(Score.getEnergy());
			level.text = String(Score.getLevel());
			skillpt.text = String(Score.getSkillPts());
			gold.text = String(Score.getGold());
			
			blastLevel.text = String(BlastManager.currentLevel);
			multishotLevel.text = String(MultishotManager.currentLevel);
			missilesLevel.text = String(MissilesManager.currentLevel);
			
			if (Score.getSkillPts() > 0) {
				if (BlastManager.currentLevel < 3) {
					blastIcon.addEventListener(Event.TRIGGERED, onBlastIconClick);
					blastIcon.touchable = true;
					blastIcon.alpha = 1;
				}
				if (MultishotManager.currentLevel < 3) {
					multishotIcon.addEventListener(Event.TRIGGERED, onMultishotIconClick);
					multishotIcon.touchable = true;
					multishotIcon.alpha = 1;
				}
				if (MissilesManager.currentLevel < 3) {
					missilesIcon.addEventListener(Event.TRIGGERED, onMissilesIconClick);
					missilesIcon.touchable = true;
					missilesIcon.alpha = 1;
				}
			} else {
				blastIcon.touchable = false;
				blastIcon.alpha = .4;
				multishotIcon.touchable = false;
				multishotIcon.alpha = .4;
				missilesIcon.touchable = false;
				missilesIcon.alpha = .4;
			}
		}
		
		public function destroy():void {
			blastIcon.removeEventListener(Event.TRIGGERED, onBlastIconClick);
			multishotIcon.removeEventListener(Event.TRIGGERED, onMultishotIconClick);
			missilesIcon.removeEventListener(Event.TRIGGERED, onMissilesIconClick);
		}
	}
}