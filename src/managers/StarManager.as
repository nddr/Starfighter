package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import objects.Star;
	import states.Play;
	
	public class StarManager {
		
		private var play:Play;
		public var stars:Array;
		private var pool:StarlingPool;
	
		public function StarManager(play:Play) {
			this.play = play;
			stars = new Array();
			pool = new StarlingPool(Star, 100);
		}
		
		public function update():void {
			if (Math.random() * 10 > 9.5) {
				spawn();
			}
			
			var s:Star;
			var len:int = stars.length;
			
			for (var i:int = len-1; i >= 0; i--) {
				s = stars[i];
				s.x -= 1;
				
				if (s.x < 0) {
					destroyStar(s);
				}
			}
		}
		
		private function spawn():void {
			var s:Star = pool.getSprite() as Star;
			
			play.addChild(s);
			s.x = 800;
			s.y = Math.random()*400;
			s.scaleX = Math.random();
			s.scaleY = Math.random();
			s.alpha = Math.random();
			
			stars.push(s);
		}
		
		public function destroyStar(b:Star):void {
			var len:int = stars.length;
			
			for (var i:int = 0; i < len; i++) {
				if (stars[i] == b) {
					stars.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		public function destroy():void {
			pool.destroy();
			pool = null;
			stars = null;
		}
	}
}