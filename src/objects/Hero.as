package objects
{
	import flash.ui.Keyboard;
	
	import core.Assets;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.KeyboardEvent;
	import starling.extensions.PDParticleSystem;
	
	import states.Play;
	
	public class Hero extends Sprite {
		
		private var play:Play;
		
		private const SPEED:Number = .70;
		private const FRIC:Number = .85;
		private const MAX_SPEED:Number = 9;
		
		private var velX:Number = 0;
		private var velY:Number = 0;
		
		private var up:Boolean;
		private var down:Boolean;
		private var left:Boolean;
		private var right:Boolean;
		private var exhaust:PDParticleSystem;
		
		public function Hero(play:Play) {
			this.play = play;
			
			var img:Image = new Image(Assets.ta.getTexture("hero"));
			img.pivotX = img.width * 0.5;
			img.pivotY = img.height * 0.5;
			x = 200;
			y = 200;
			addChild(img);
			
			exhaust = new PDParticleSystem(XML(new Assets.exhaustXML()),
				Assets.ta.getTexture("particle"));
			Starling.juggler.add(exhaust);
			play.addChild(exhaust);
		}
		
		public function update():void {
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			Starling.current.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			exhaust.emitterX = x - 20;
			exhaust.emitterY = y;
			
			if (left) { 
				velX -= SPEED;
			} else if (right) { 
				velX += SPEED;
				exhaust.start(3);
			} else { 
				velX *= FRIC;
			}
			
			if (up) { 
				velY -= SPEED;
				scaleY = scaleY * .98;
			} else if (down) { 
				velY += SPEED;
				scaleY = scaleY * .98;
			} else {
				velY *= FRIC;
			}
			
			scaleY = (MAX_SPEED - Math.abs(velY)) / (MAX_SPEED * 4) + 0.75;
			
			//Keep hero from moving faster than MAX_SPEED
			if (velX > MAX_SPEED) {
				velX = MAX_SPEED;
			} else if (velX < -MAX_SPEED) {
				velX = -MAX_SPEED;
			}
			
			if (velY > MAX_SPEED) {
				velY = MAX_SPEED;
			} else if (velY < -MAX_SPEED) {
				velY = -MAX_SPEED;
			}
			
			//Keep hero from moving off screen
			if (x > 776 ) {
				x = 776;
			}
			
			if (x < 24) {
				x = 24;
			}
			
			if (y > 376) {
				y = 376;
			}
			
			if (y < 24) {
				y = 24;
			}
			
			x += velX; 
			y += velY;
		}
		
		private function onKeyUp(e:KeyboardEvent):void {
			switch (e.keyCode) {
				case Keyboard.LEFT :
					left = false;
					break;
				case Keyboard.RIGHT :
					right = false;
					break;
				case Keyboard.UP :
					up = false;
					break;
				case Keyboard.DOWN :
					down = false;
					break;
			}
		}
		
		private function onKeyDown(e:KeyboardEvent):void {
			switch (e.keyCode) {
				case Keyboard.LEFT :
					left = true;
					break;
				case Keyboard.RIGHT :
					right = true;
					break;
				case Keyboard.UP :
					up = true;
					break;
				case Keyboard.DOWN :
					down = true;
					break;
			}
		}
	}
}